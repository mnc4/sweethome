import { HttpClient  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { environment as ENV } from '../../env/env' ;

/*
  Generated class for the MyServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyServiceProvider {
	transactions=[];
	token="";
	API_endpoint;
	//API_endpoint="http://sweethome.mnc4.net/";
	//API_endpoint="http://localhost/be/sweethome/public/";
	//API_endpoint="http://192.168.0.27/be/sweethome/public/";

	constructor(public http: HttpClient, private storage: Storage) {
		this.API_endpoint=ENV.API_endpoint;
	}

	getToken(user,password) {
		
		let token = this.http.post(this.API_endpoint+"api/get_token",{email:user,password:password});
        return token;
	}

	setToken(token) {
		this.storage.set('token', token);
		this.token=token;
	}

	saveTokenFromLocal(val){
		this.token=val;
	}

	addTransaction(name,value) {
		let transaction = this.http.post(this.API_endpoint+"api/add_transaction?token="+this.token,{name:name,value:value});
        return transaction;
	}

	isTokenValid(token) {
		let transactions = this.http.get(this.API_endpoint+"api/transactions"+"?token="+token);
        return transactions;
	}

	getTransactions() {
		let transactions = this.http.get(this.API_endpoint+"api/transactions"+"?token="+this.token);
        return transactions;
	}

	getTransaction(id) {
        let transaction = this.http.get(this.API_endpoint+"api/add_transaction/"+id);
        return transaction;
	}

	deleteTransaction(id) {
        let transaction = this.http.get(this.API_endpoint+"api/delete_transaction/"+id);
        return transaction;
	}

	saveTransaction(id,name,value,date) {
        let transaction = this.http.post(this.API_endpoint+"api/save_transaction/"+id,{name:name,value:value,date:date});
        return transaction;
	}

	setTransactions(transactions) {
		this.transactions=transactions;
	}
}