import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { LoadingController } from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';

import { IonicStorageModule } from '@ionic/storage';

import { OrderBy } from '../filter/orderBy';
import { Search } from '../filter/search';

import { MyApp } from './app.component';

import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { TransactionDetailPage } from '../pages/transaction-detail/transaction-detail';
import { TransactionsPage } from '../pages/transactions/transactions';
import { TransactionNewPage } from '../pages/transaction-new/transaction-new';
import { MyServiceProvider } from '../providers/my-service/my-service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TransactionDetailPage,
    TransactionsPage,
    TransactionNewPage,
    LoginPage,
    OrderBy,
    Search
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TransactionDetailPage,
    TransactionsPage,
    TransactionNewPage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MyServiceProvider,
    LoadingController,
    DatePicker,
    Search
  ]
})
export class AppModule {}
