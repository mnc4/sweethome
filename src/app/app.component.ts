import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';

import { MyServiceProvider } from '../providers/my-service/my-service';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  //rootPage:any = HomePage;
  rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private myService: MyServiceProvider, private storage: Storage) {
    platform.ready().then(() => {

      this.storage.get('token').then((val) => {
          this.myService.isTokenValid(val)
          .subscribe
          (
            data => {
              this.rootPage=HomePage;
              this.myService.saveTokenFromLocal(val);
              statusBar.styleDefault();
              splashScreen.hide();
            },
            error => {
              this.rootPage=LoginPage;
              statusBar.styleDefault();
              splashScreen.hide();
            }
          );
      });
      
      
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      
      
    });
  }
}

