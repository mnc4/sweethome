import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TransactionsPage } from '../transactions/transactions';
import { TransactionNewPage } from '../transaction-new/transaction-new';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  indexOfLogin;
  hideButton=false;

  constructor(public navCtrl: NavController,public navParams: NavParams) {
    this.indexOfLogin=navParams.get('indexOfLogin');
    if(this.indexOfLogin){
      navCtrl.remove(this.indexOfLogin-1,1);
      this.hideButton=true;
    }

  }

  ionViewWillEnter() {
  }

  goTo(page) {
  	if(page=='tranactions') {
  		this.navCtrl.push(TransactionsPage);
  	} else if(page=='transactionNew') {
  		this.navCtrl.push(TransactionNewPage);
  	}
  }

  ngOnInit(){
  }

}
