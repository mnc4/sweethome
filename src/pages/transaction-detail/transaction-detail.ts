import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

import { HomePage } from '../home/home';

import { MyServiceProvider } from '../../providers/my-service/my-service';
/**
 * Generated class for the TransactionDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-transaction-detail',
  templateUrl: 'transaction-detail.html',
})
export class TransactionDetailPage {
	name;
	value;
	date;
	id;
	loading;
	constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController, private myService: MyServiceProvider) {
		this.id=navParams.get('id');
		this.loading=loadingCtrl.create({
			content: 'Please wait...'
		});
		this.loading.present();

		myService.getTransaction(this.id)
		.subscribe
		(
			data => {
				let transaction=data[0];
				console.log(transaction);
				this.name=transaction.name;
				this.value=transaction.value;
				this.date=transaction.date.split(" ")[0];
				this.loading.dismiss();
			}
		);
	}

	deleteTransaction() {
		this.loading=this.loadingCtrl.create({
			content: 'Please wait...'
		});
		this.loading.present();
		this.myService.deleteTransaction(this.id)
		.subscribe
		(
			data => {
				this.loading.dismiss();
				this.navCtrl.push(HomePage);
			}
		);
	}

	saveTransaction() {
		this.loading=this.loadingCtrl.create({
			content: 'Please wait...'
		});
		this.loading.present();
		this.myService.saveTransaction(this.id,this.name,this.value,this.date)
		.subscribe
		(
			data => {
				this.loading.dismiss();
				//this.navCtrl.push(HomePage);
			}
		);
	}
}
