import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

import { Search } from '../../filter/search';

import { TransactionDetailPage } from '../transaction-detail/transaction-detail';

import { MyServiceProvider } from '../../providers/my-service/my-service';
/**
 * Generated class for the TransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-transactions',
  templateUrl: 'transactions.html',
})
export class TransactionsPage {
	transactions;
	total;
	loading;

	field = 'date';
    descending = false;
    sorting='date';

    searchField = '';

	constructor(public navCtrl: NavController, public navParams: NavParams, private myService: MyServiceProvider, private loadingCtrl: LoadingController, private search: Search) {
	}

	goTo(id) {
		this.navCtrl.push(TransactionDetailPage,{id:id});
	}

	calculateTotal() {
		this.total=0;
		this.search.transform(this.transactions,this.searchField).forEach(
			transaction => {
				this.total = this.total + Number(transaction.value);
			}
		);
	}

	ionViewWillEnter() {

		this.loading=this.loadingCtrl.create({
			content: 'Please wait...'
		});
		this.loading.present();


		this.myService.getTransactions()
		.subscribe
		(
			data => {
				let transactions: any;
				transactions = data;
				for(let i=0;i<transactions.length;i++){
					transactions[i].date_day=transactions[i].date.split(" ")[0];
				}
				this.transactions=transactions;
				this.calculateTotal();
				this.myService.setTransactions(data);
				this.loading.dismiss();
			}
		);
	}

	sortBy(field){
		if(field==this.field){
			this.descending=!this.descending;
			if(this.descending==true) {
				this.sorting = "-"+field;
			} else {
				this.sorting = field;
			}
		} else {
			this.descending=false;
			this.field=field;
			this.sorting = field;
		}
	}

}
