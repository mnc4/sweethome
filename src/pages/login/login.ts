import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HomePage } from '../home/home';

import { MyServiceProvider } from '../../providers/my-service/my-service';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	user;
	password;
  constructor(public navCtrl: NavController, public navParams: NavParams,private myService: MyServiceProvider) {
  	
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login(){
  	this.myService.getToken(this.user,this.password)
	.subscribe
	(
		data => {
			let token:any;
			token=data;
			this.myService.setToken(token.token);

			let my_self=this.navCtrl.length();
			this.navCtrl.push(HomePage,{indexOfLogin:my_self});			
		},
		error => {
			this.user="";
			this.password="";
			this.myService.setToken("");
		}
	);
  }


}
