import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MyServiceProvider } from '../../providers/my-service/my-service';
import { LoadingController } from 'ionic-angular';

/**
 * Generated class for the TransactionNewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-transaction-new',
  templateUrl: 'transaction-new.html',
})
export class TransactionNewPage {
	name;
	value;
	loading;
	constructor(public navCtrl: NavController, public navParams: NavParams, private myService: MyServiceProvider,  private loadingCtrl: LoadingController) {
	}

	addTransiction() {
		this.loading=this.loadingCtrl.create({
			content: 'Please wait...'
		});
		this.loading.present();
		this.myService.addTransaction(this.name,this.value)
		.subscribe
		(
			data => {
				this.loading.dismiss();
				this.name=null;
				this.value=null;
			}
		);
	}

	valid() {
		if(this.name && this.value) {
			return true;
		} else {
			return false;
		}
	}
	ngOnInit() {
		console.log(this.myService.transactions);
	}
}
