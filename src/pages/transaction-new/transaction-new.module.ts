import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransactionNewPage } from './transaction-new';

@NgModule({
  declarations: [
    TransactionNewPage,
  ],
  imports: [
    IonicPageModule.forChild(TransactionNewPage),
  ],
})
export class TransactionNewPageModule {}
